package controllers

import (
	"encoding/json" // golang ga secara default mengerti json. makanya digunain package encoing/json
	"fmt"           //buat print dll
	"mongo-golang/models"
	"net/http"

	"github.com/julienschmidt/httprouter"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type UserController struct {
	session *mgo.Session
}

func NEwUserController(s *mgo.Session) *UserController {
	return &UserController{s}
}

func (uc UserController) GetUser(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	id := p.ByName("id")

	//buat cek idnya sesuai ngga
	if !bson.IsObjectIdHex(id) {
		w.WriteHeader(http.StatusNotFound)
	}
	oid := bson.ObjectIdHex(id)
	u := models.User{
		Id:     "",
		Name:   "",
		Gender: "",
		Age:    0,
	}

	if err := uc.session.DB("mongo-golang").C("users").FindId(oid).One(&u); err != nil {
		w.WriteHeader(404)
		return
	}
	uj, err := json.Marshal(u) // marshal dengan variabel uj. marshaling adalah convrting to atau from json
	if err != nil {
		fmt.Println(err)
	}
	w.Header().Set("Content-Type", "application/json") //ngaish tau bakal ngirim data json
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "%s\n", uj) //sent to frond end as json
}

// -----------
// Mthod Create
// -----------

func (uc UserController) CreatUser(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	u := models.User{
		Id:     "",
		Name:   "",
		Gender: "",
		Age:    0,
	}

	json.NewDecoder(r.Body).Decode(&u) // decode json value from postman supaya bisa dipake golang
	// bakal di decode disimpan di variabel u
	u.Id = bson.NewObjectId()
	uc.session.DB("mongo-golang").C("users").Insert(u)

	uj, err := json.Marshal(u) //ubah kebentuk yang bisa dibaca user
	if err != nil {
		fmt.Println(err)
	}
	w.Header().Set("Content-Type", "application.json")
	w.WriteHeader(http.StatusCreated)
	fmt.Fprintf(w, "%s\n", uj)
}

func (uc UserController) DeleteUser(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	id := p.ByName("id")

	if !bson.IsObjectIdHex(id) {
		w.WriteHeader(404)
		return
	}
	oid := bson.ObjectIdHex(id)

	if err := uc.session.DB("mongo-golang").C("users").RemoveId(oid); err != nil {
		w.WriteHeader(404)
	}
	w.WriteHeader(http.StatusOK)
	fmt.Fprint(w, "Deleted user", oid, "\n")
}
