package main

import (
	"mongo-golang/controllers"
	"net/http"

	"github.com/julienschmidt/httprouter"
	"gopkg.in/mgo.v2"
)

func main() {
	r := httprouter.New() //buat new instance. ini buat variabel r. mempermudah pnulisan
	uc := controllers.NEwUserController(getSession())
	r.GET("/user/:id", uc.GetUser) //din handler sm mongoDB
	r.POST("/user", uc.CreatUser)
	r.DELETE("/user/:id", uc.DeleteUser)
	http.ListenAndServe("localhost:9000", r) // localhost yg bakal dipake
}

func getSession() *mgo.Session { // () -> input param *mgo.session -> returnnya
	s, err := mgo.Dial("mongodb://localhost:27017")
	if err != nil {
		panic(err)
	}
	return s // kalo ga error bakal return s. kalo error bakal ke panic(err)
}

// package main

// import (
// 	"fmt"
// 	"time"

// 	"labix.org/v2/mgo"
// )

// func connectToMongo() bool {
// 	ret := false
// 	fmt.Println("enter main - connecting to mongo")

// 	// tried doing this - doesn't work as intended
// 	defer func() {
// 		if r := recover(); r != nil {
// 			fmt.Println("Detected panic")
// 			var ok bool
// 			err, ok := r.(error)
// 			if !ok {
// 				fmt.Printf("pkg:  %v,  error: %s", r, err)
// 			}
// 		}
// 	}()

// 	maxWait := time.Duration(5 * time.Second)
// 	session, sessionErr := mgo.DialWithTimeout("localhost:27017", maxWait)
// 	if sessionErr == nil {
// 		session.SetMode(mgo.Monotonic, true)
// 		coll := session.DB("MyDB").C("MyCollection")
// 		if coll != nil {
// 			fmt.Println("Got a collection object")
// 			ret = true
// 		}
// 	} else { // never gets here
// 		fmt.Println("Unable to connect to local mongo instance!")
// 	}
// 	return ret
// }

// func main() {
// 	if connectToMongo() {
// 		fmt.Println("Connected")
// 	} else {
// 		fmt.Println("Not Connected")
// 	}
// }
