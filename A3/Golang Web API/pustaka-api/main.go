package main

import (
	"pustaka-api/book"
	//"encoding/json"
	//"net/http"
	"github.com/gin-gonic/gin"
	//"github.com/go-playground/validator/v10"
	"log"
	//"fmt"
	"pustaka-api/handler" //buat ngakses package handler
	"gorm.io/driver/mysql"
  	"gorm.io/gorm"
)
func main() {
	dsn := "root:@tcp(127.0.0.1:3306)/pustaka-api?charset=utf8mb4&parseTime=True&loc=Local"
 	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatal("Db connection error")
	}

	db.AutoMigrate(&book.Book{}) // auto migration

	bookRepository := book.NewRepository(db)
	bookService := book.NewService(bookRepository)
	bookHandler := handler.NewBookHandler(bookService)

	router := gin.Default()

	v1 := router.Group("/v1") // membuat versi
	
	//diubah untuk bisa diakses service
	v1.GET("/books", bookHandler.GetBooks)
	v1.POST("/books", bookHandler.CreateBook) // ini buat input data
	v1.GET("/books/:id",bookHandler.GetBook)
	v1.PUT("/books/:id", bookHandler.UpdateBook)
	v1.DELETE("/books/:id", bookHandler.DeleteBook)	
	router.Run() //port default 8080
	// kalo mau portnya diatur router.Run(":8888")
}

// main
// handler
// service
// repository
// db
// mysql