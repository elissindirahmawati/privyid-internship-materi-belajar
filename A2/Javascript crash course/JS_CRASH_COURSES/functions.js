// Functions
//1 
function addNums(num1, num2) {
    console.log(num1 + num2);
}
addNums(1,2);

//2
function addNums1(num1=3, num2=2) {
    console.log(num1 + num2);
}
addNums1(4,2); // yg diprint tetep yang ini bukan yg atas

//3
function addNums2(num1 = 1, num2=2) {
    return num1+num2;
}
console.log(addNums2(5,5));

//4 arrow function
//4.1
const addNums3 = (num1 = 1, num2=2) => 
console.log(num1+num2);
addNums3(7,4);

//4.2
const addNums4 = (num1=1, num2=2) =>
num1+num2;
console.log(addNums4(5,7));

//4.3
const addNums5 = (num1=1, num2=2) => {
    return num1+num2;
}
console.log(addNums5(5,9));

