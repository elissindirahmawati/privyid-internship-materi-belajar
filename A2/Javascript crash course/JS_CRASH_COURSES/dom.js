const ul = document.querySelector('.items'); // ambil data dari class items
//ul.lastElementChild.remove(); // hapus baian terakhir dari ul
//ul.remove(); // hapus ul
console.log(ul);
ul.firstElementChild.textContent = 'Hello'; // mengisi elemen pertama dengan hello
ul.children[1].innerText = "Brad";
ul.lastElementChild.innerHTML = '<h1>Hello</h1>';

//memberikan style
const btn = document.querySelector('.btn');
btn.style.background = 'red'; //memberikan background merah dengan class btn



//Event 
const button = document.querySelector('.btn');
button.addEventListener('click', (e) => { //(e) itu  maksudnya fungsi event
    e.preventDefault();//supaya dia teep tinggal ga ilang2
    console.log('click');
    console.log(e);
    });
const button1 = document.querySelector('.btn');
button1.addEventListener('mouseover', (e) => { //(e) itu  maksudnya fungsi event
    e.preventDefault();//supaya dia teep tinggal ga ilang2
    document.querySelector('#my-form').style.background = '#CCC'; // mengubah warna background
    document.querySelector('body').classList.add('bg-dark');
    document.querySelector('.items')
    .lastElementChild.innerHTML = '<h1>hello</h1>';
});