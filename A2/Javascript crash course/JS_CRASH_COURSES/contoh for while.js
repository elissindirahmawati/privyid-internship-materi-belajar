// object in array
const todos = [
    {
        id: 1,
        text: 'take out trash',
        isCompleted: true
    },
    {
        id: 2,
        text: 'meeting with boss',
        isCompleted: true
    },
    {
        id: 3,
        text: 'dentist appt',
        isCompleted: false
    },
]; 

// For
for(let i = 0; i < 10; i ++) {
    console.log(i);
    console.log(`For Loop Number: ${i}`)
    }
    
// While
let i = 0;
while(i < 10) {
    console.log(`While Loop Number: ${i}`);
    i++;
}

//for praktik
for(let todo of todos) {
    console.log(todo.text);
}
// forEach, map, filter
todos.forEach(function(todo) {
console.log(todo.text); 
})

const todoText = todos.map(function(todo) {
return todo.text;
});

const todoCompleted = todos.filter(function(todo) {
    return todo.isCompleted === true;
    }).map(function(todo) {
        return todo.text;
    });

console.log(todoCompleted);