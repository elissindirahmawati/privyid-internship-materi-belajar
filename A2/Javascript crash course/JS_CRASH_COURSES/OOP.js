
//Object Oriented Programming
// Cara 1
//constructor function
function Person(firstName, LastName, dob) {
    this.firstName= firstName;
    this.LastName = LastName;
    this.dob = new Date(dob); //biar jadi tanggal karena sebelumnya string
    /*this.getBirthYear = function() {
        return this.dob.getFullYear();
    }
    this.getFullName = function() {
        return `${this.firstName} ${this.LastName}`;
    }*/
    }
    
    Person.prototype.getBirthYear = function() {
        return this.dob.getFullYear();
    }
    
    Person.prototype.getFullName = function() {
        return `${this.firstName} ${this.LastName}`
    }
    /*
    //Cara 2
    //Class constructor-> di bawah ini adalah cara lain untuk hal yang sama
    class Person {
        constructor(firstName, LastName, dob) {
            this.firstName = firstName;
            this.LastName = LastName;
            this.dob = new Date(dob);
        }
        getBirthYear() {
            return  this.dob.getFullYear();
        }
        getFullName() {
            return `${this.firstName} ${this.LastName}`;
        }
    }
    */
    
    //instantiate object
    const person1 = new Person('John', 'Doe', '4/3/1980');
    const person2 = new Person('Mary', 'Smith', '3/6/1970');
    
    console.log(person2.dob);
    console.log(person2.dob.getFullYear());
    console.log(person1.getBirthYear());
    console.log(person1.getFullName());
    
    