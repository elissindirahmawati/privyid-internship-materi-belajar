const myForm = document.querySelector('#my-form');
const nameInput = document.querySelector('#name'); //input name
const emailInput = document.querySelector('#email'); //input email
const msg = document.querySelector('.msg'); //message
const userList = document.querySelector('#users');

myForm.addEventListener('submit', onSubmit); //menambahkan event ketika submit

function onSubmit(e) { //fungsi
    e.preventDefault(); 

    console.log(nameInput.value); //menampilkan value dari nameInput di console log
    if(nameInput.value === '' || emailInput.value === '') { // Kalo nameInput atau emailInput kosong. maka keluarkan notifikasi
        //alert('Please enter fields'); //ini pake alert. kalo kosong bakal keluar alert 'Please enter fields'
        msg.classList.add('error'); // ini pake message. stylenya ambil dari style.css ada class namanya error
        msg.innerHTML = 'Please enter all fields'; //isi messagenya
        setTimeout(() => msg.remove(), 3000) //set supaya notifnya ilang atau terhapus setelah 3 detik.
    } else {
        const li = document.createElement('li'); //membuat element baru bernama li
        li.appendChild(document.createTextNode(
            `${nameInput.value} : ${emailInput.value}`));

        userList.appendChild(li); //menambahkan element li ke userlist

        //clear field
        nameInput.value = '';
        emailInput.value = '';

    }
}
