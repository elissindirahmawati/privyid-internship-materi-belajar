// Conditional
const x = 6; 
const y = 10;

if(x == 10){ 
console.log('x is 10');
} else if(x > 10) {
    console.log('x is greater than 10')
}else {
    console.log('x is NOT 10');
}

if(x > 5 || y > 10){  // or
    console.log('x is more than 5 and y is more than 10'); 
}

if(x > 5 && y > 10){  // and
    console.log('x is more than 5 and y is more than 10'); 
}

if(x > 5) {
    if(y > 10) {
    }
}  

//masih conditional
const a = 11;
const color = a > 10 ? 'red' : 'blue';
console.log(color);

// switch case
switch(color) {
    case 'red':
        console.log('color is red');
        break;
        case 'blue':
            console.log('color is blue');
            break;
            default:
                console.log('color is NOT red or blue');
                break;
}